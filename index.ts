
import Koa from 'koa'
import Router from 'koa-router'
const router = new Router()
import * as view from './view'
import main from './main'
import fs from 'fs'
import * as dotenv from 'dotenv'
dotenv.config()
const config = process.env
router.get('/', async (ctx, next) => {
	ctx.body = `<h1>KukaiBotPDF</h1>`;
})
router.get('/pdf/:group/:id', async (ctx, next) => {
	const { pwd } = ctx.request.query
	if (pwd !== config.SYSTEM_PASSWORD) return false
	const page = await view.getPage(ctx.params.id, ctx.params.group)
	await main(ctx.params.id, page, ctx.params.group)
	ctx.body = `success`;
})
router.get('/pdf/:group/:id/:page', async (ctx, next) => {
	const { pwd } = ctx.request.query
	if (pwd !== config.SYSTEM_PASSWORD) return false
	await main(ctx.params.id, ctx.params.page, ctx.params.group)
	ctx.body = `success`;
})
router.get('/view/all/:group/:id', async (ctx, next) => {
	const { pwd } = ctx.request.query
	if (pwd !== config.INTERNAL_PASSWORD) return false
	ctx.body = await view.allView(ctx.params.id, ctx.params.group, false)
})
router.get('/view/all/:group/:id/revealed', async (ctx, next) => {
	const { pwd } = ctx.request.query
	if (pwd !== config.INTERNAL_PASSWORD) return false
	ctx.body = await view.allView(ctx.params.id, ctx.params.group, true)
})
router.get('/view/:group/:id/:page', async (ctx, next) => {
	const { pwd } = ctx.request.query
	if (pwd !== config.INTERNAL_PASSWORD) return false
	ctx.body = await view.view(ctx.params.id, ctx.params.page, ctx.params.group, false, false)
})
router.get('/view/:group/:id/:page/revealed', async (ctx, next) => {
	const { pwd } = ctx.request.query
	if (pwd !== config.INTERNAL_PASSWORD) return false
	ctx.body = await view.view(ctx.params.id, ctx.params.page, ctx.params.group, true, false)
})
router.get('/static/:type/:asset', async (ctx, next) => {
	const { asset, type } = ctx.params
    let mime = ''
	if (type === 'css') mime = 'text/css'
	if (type === 'fonts') mime = 'application/x-font-otf'
    ctx.set('Content-Type', mime)
    ctx.body = fs.readFileSync(`${asset}`).toString()
})
const app = new Koa()
app.use(router.routes()).use(router.allowedMethods())
const dev = false
app.listen(dev ? 8819 : 8080, () => {
    console.log('Server started!!')
})
