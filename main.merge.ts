import AWS from 'aws-sdk'
import fs from 'fs'
import puppeteer from 'puppeteer'
import PDFMerger from 'pdf-merger-js'
import * as dotenv from 'dotenv'
dotenv.config()
const config = process.env

const host = config.HOST
const bucket = config.BUCKET
const env = config.ENV

const marginprop = { top: '10mm', left: '10mm', bottom: '10mm', right: '10mm' }
const main = async function (id: string, pageStr: string, group: string) {
	const page = parseInt(pageStr, 10)
	const mergerNormal = new PDFMerger()
	const mergerRevealed = new PDFMerger()
	let prefix = group
	if (group == 'rn') prefix = ''
	for (let i = 1; i <= page; i++) {
		await makeRevealedPdf(id, i, group)
		mergerRevealed.add(`${prefix}kukai${id}.${i}.revealed.pdf`)
		await makeNormalPdf(id, i, group)
		mergerNormal.add(`${prefix}kukai${id}.${i}.pdf`)
	}
	await mergerNormal.save(`${prefix}kukai${id}.pdf`)
	await mergerRevealed.save(`${prefix}kukai${id}.revealed.pdf`)
	for (let i = 1; i <= page; i++) {
		fs.unlink(`${prefix}kukai${id}.${i}.pdf`, vd)
		fs.unlink(`${prefix}kukai${id}.${i}.revealed.pdf`, vd)
	}
	upload(`${prefix}kukai${id}.pdf`)
	upload(`${prefix}kukai${id}.revealed.pdf`)
}
export default main
function vd() { }
async function makeNormalPdf(id: string, i: number, group: string) {
	let prefix = group
	if (group == 'rn') prefix = ''
	const browser = await puppeteer.launch({
		headless: env !== 'development',
		args: [
			'--no-sandbox',
			'--disable-setuid-sandbox'
		]
	})
	const page = await browser.newPage()
	await page.goto(`https://${host}/view/${group}/${id}/${i}?pwd=${config.INTERNAL_PASSWORD}`, { waitUntil: 'networkidle0' })
	await page.pdf({
		path: `${prefix}kukai${id}.${i}.pdf`,
		landscape: true,
		width: '257mm',
		height: '182mm',
		margin: marginprop
	})
	await page.evaluateHandle('document.fonts.loadingend')
	browser.close()
}
async function makeRevealedPdf(id: string, i: number, group: string) {
	let prefix = group
	if (group == 'rn') prefix = ''
	const browser = await puppeteer.launch({
		args: [
			'--no-sandbox',
			'--disable-setuid-sandbox'
		]
	})
	const page = await browser.newPage()
	//await page.goto(`https://${host}/pdf/pdf.core.php?id=${id}&page=${i}&revealed=true&group=${group}`, { waitUntil: 'networkidle2' })
	await page.goto(`https://${host}/view/${group}/${id}/${i}/revealed?pwd=${config.INTERNAL_PASSWORD}`, { waitUntil: 'networkidle2' })
	await page.pdf({
		path: `${prefix}kukai${id}.${i}.revealed.pdf`,
		landscape: true,
		width: '257mm',
		height: '182mm',
		margin: marginprop
	})
	await page.evaluateHandle('document.fonts.loadingend')
	browser.close()
}

function upload(file: string) {
	if (!bucket) return false
	AWS.config.loadFromPath('./rootkey.json')
	AWS.config.update({ region: 'ap-northeast-1' })

	const s3 = new AWS.S3()
	const v = fs.readFileSync(file)
	const params = {
		Bucket: bucket,
		Key: file,
		Body: v
	}

	console.log(params)

	s3.putObject(params, function (err, data) {
		if (err) console.log(err, err.stack)
		else console.log(data)
		fs.unlink(file, vd)
	})
}
