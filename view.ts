import * as mysql from 'mysql2/promise'
import knex from 'knex'
import fs from 'fs'
import * as dotenv from 'dotenv'
dotenv.config()
const config = process.env
const dbConfig = {
    host: config.DB_HOST,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    database: config.DB_DATABASE,
}
const pool = async () => {
    const db = await mysql.createPool(dbConfig)
    return db
}
const my = knex({ client: 'mysql' })

export const allView = async function (id: string, group: string, revealed: boolean) {
    try {
        const pageStr = await getPage(id, group)
        const page = parseInt(pageStr, 10)
        const html = []
        for (let i = 1; i <= page; i++) {
            html.push(await view(id, i.toString(), group, revealed, true))
        }
        const header = fs.readFileSync('./template.header.html').toString()
        return header + html.join('')
    } catch (e: any) {
        console.error(e)
    }
}
export const view = async function (id: string, pageStr: string, group: string, revealed: boolean, headless: boolean) {
    const db = await pool()
    const sql = my(`${group}_kukai`).select().where('ID', id).toString()
    let kukai
    try {
        console.log(sql)
        let [kukais] = await db.query(sql) as any
        kukai = kukais[0]
    } catch (err) {
        console.error(err)
        return 'error11'
    }
    const sql2 = my(`${group}_haiku`).select().where('Kukai', id).orderBy('Code', 'desc').toString()
    let result
    try {
        [result] = await db.query(sql2) as any
    } catch (err) {
        console.error(err)
        return 'error2'
    }
    const page = parseInt(pageStr, 10)
    const end = page * 10 - 1
    const start = end - 9
    let temp = fs.readFileSync('./template.core.html').toString()
    let ct = 0
    let tome = false
    for (let i = start; i <= end; i++) {
        if (!result[i]) {
            temp = temp.replace(`%ku${ct}%`, '').replace(`%people${ct}%`, '')
            tome = true
            ct++
            continue
        }
        const ku = result[i]
        temp = temp.replace(`%ku${ct}%`, ku.Haiku)
        if (revealed) {
            const people = await idToName(group, ku.User, db)
            temp = temp.replace(`%people${ct}%`, people.substr(0, 4))
        } else {
            temp = temp.replace(`%people${ct}%`, '')
        }
        ct++
    }
    const fillTome = tome ? 'トメ' : ''
    temp = temp.replace(`%title%`, kukai.Name).replace(`%page%`, toZenkaku(page)).replace(`%tome%`, fillTome)
    const header = fs.readFileSync('./template.header.html').toString()
    if (!headless) temp = header + temp
    return temp
}
export const getPage = async function (id: string, group: string) {
    const db = await pool()
    const sql = my(`${group}_haiku`).select().where('Kukai', id).orderBy('Code', 'desc').toString()
    let result
    try {
        [result] = await db.query(sql) as any
    } catch (err) {
        console.error(err)
        return 'error3'
    }
    return Math.ceil(result.length / 10).toString()
}

const idToName = async (prefix: string, i: string, db: any) => {
    const sql = my(`${prefix}_name`).select('Name').where('User', i).toString()
    try {
        const [result] = await db.query(sql) as any
        return result[0]['Name']
    } catch (err) {
        return null
    }
}
const toZenkaku = (page: number) => {
    if (page === 1) return '１'
    if (page === 2) return '２'
    if (page === 3) return '３'
    if (page === 4) return '４'
    if (page === 5) return '５'
    if (page === 6) return '６'
    if (page === 7) return '７'
    if (page === 8) return '８'
    if (page === 9) return '９'
    return page.toString()
}
